[![pipeline status](https://gitlab.com/open-source-devex/terraform-modules/aws/ecs-alb/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/terraform-modules/aws/ecs-alb/commits/master)

# ECS ALB

Terraform module for creating an ALB integrated with an ECS cluster.
The ALB will provide by default two target groups:
* HTTP to HTTPS redirection on the port 80 listener
* a maintenance page as the default rule of the 443 port listener

As services get added to the ALB they will register themselves as new target groups of the 443 port listener.
To put a service in maintenance mode all one needs to do is to remove the respective listener rule.
That will make requests for that service match the default rule of the 443 port listener which will serve the maintenance page.

This module works with version `v1.0.0` opf the [acme provider](https://github.com/vancluever/terraform-provider-acme).
