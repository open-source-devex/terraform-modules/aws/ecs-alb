resource "aws_lb_target_group" "https_default" {
  count = "${var.default_page_static_content == "" ? 1 : 0}"

  name     = "${local.resource_name_prefix}HTTPS"
  port     = "${var.default_page_container_port}"
  protocol = "${var.target_group_protocol}"
  vpc_id   = "${var.vpc_id}"

  deregistration_delay = "${var.deregistration_delay}"

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    interval            = "${var.health_check_interval}"
    timeout             = "${var.health_check_timeout}"
    path                = "${var.default_page_health_check_path}"
    matcher             = "${var.default_page_health_check_matcher}"
  }

  tags {
    Name        = "${title(var.project)} ${title(var.environment)} ${var.internal ? "internal" : "public"} HTTPS Default Target Group"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_lb.default"]
}

variable "default_page" {
  default = "defaultPage"
}

resource "aws_ecs_service" "https_default" {
  count = "${var.default_page_static_content == "" ? 1 : 0}"

  name            = "${var.default_page}"
  cluster         = "${var.ecs_cluster_id}"
  task_definition = "${aws_ecs_task_definition.https_default.arn}"
  desired_count   = "${var.default_task_count}"

  iam_role = "${var.aws_role_ecs_service_arn}"

  deployment_minimum_healthy_percent = "${var.service_minimum_healthy}"
  deployment_maximum_percent         = "${var.service_deployment_maximum}"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.https_default.arn}"
    container_name   = "${var.default_page}"
    container_port   = "${var.default_page_container_port}"
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  lifecycle {
    ignore_changes = ["aws_ecs_task_definition.https_default"]
  }

  depends_on = [
    "aws_lb_target_group.https_default",
    "aws_lb_listener.https",
  ]
}

resource "aws_ecs_task_definition" "https_default" {
  count = "${var.default_page_static_content == "" ? 1 : 0}"

  family = "${var.default_page}${title(local.resource_name_prefix)}"

  container_definitions = <<EOCONTAINERDEF
[{
  "cpu": ${var.default_page_container_cpu},
  "memory": ${var.default_page_container_memory},
  "portMappings": [{
    "hostPort": 0,
    "containerPort": ${var.default_page_container_port},
    "protocol": "tcp"
  }],
  "environment": ${replace(jsonencode(var.default_page_container_environment_configuration), "/\\\\n/", " ")},
  "essential": true,
  "name": "${var.default_page}",
  "image": "${var.default_page_container_image}:${var.default_page_container_image_version}"
}]
EOCONTAINERDEF

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${join(",", var.aws_azs)}]"
  }
}
