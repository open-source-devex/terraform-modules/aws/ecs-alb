terraform {
  required_version = ">= 0.10.13" # introduction of Local Values configuration language feature
}

locals {
  default_resource_name_prefix = "${title(var.project)}${title(var.environment)}"
  resource_name_prefix         = "${var.alb_name != "" ? var.alb_name : local.default_resource_name_prefix}"

  # Workaround for interpolation not being able to "short-circuit" the evaluation of the conditional branch that doesn't end up being used
  # Source: https://github.com/hashicorp/terraform/issues/11566#issuecomment-289417805
  log_bucket = "${element(split(",", (var.create_log_s3_bucket ? join(",", aws_s3_bucket.new.*.id) : join(",", data.aws_s3_bucket.existing.*.id))), 0)}"

  log_bucket_prefix = "${var.create_log_s3_bucket ? "" : "logs/"}${lower(local.resource_name_prefix)}"

  # Workaround for interpolation not being able to "short-circuit" the evaluation of the conditional branch that doesn't end up being used
  # Source: https://github.com/hashicorp/terraform/issues/11566#issuecomment-289417805
  certificate_arn = "${element(split(",", (var.request_letsencrypt_certificate ? join(",", aws_iam_server_certificate.default.*.arn) : var.certificate_arn)), 0)}"
}

resource "aws_lb" "default" {
  name     = "${local.resource_name_prefix}"
  internal = "${var.internal}"

  security_groups = [
    "${aws_security_group.default.id}",
    "${var.extra_security_groups}",
  ]

  subnets = ["${var.subnet_ids}"]

  enable_deletion_protection = "${var.alb_deletion_protection}"

  idle_timeout = "${var.alb_idle_timeout}"

  access_logs {
    bucket  = "${local.log_bucket}"
    prefix  = "${local.log_bucket_prefix}"
    enabled = "${var.log_bucket_enabled}"
  }

  tags {
    Name        = "${title(var.project)} ${title(var.environment)} ${var.internal ? "internal" : "public"} ALB"
    Environment = "${var.environment}"
  }

  # these two dependencies are pulled in by local.log_bucket
  depends_on = [
    "aws_s3_bucket.new",
    "data.aws_s3_bucket.existing",
  ]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = "${aws_lb.default.arn}"
  port              = "${var.listener_http_port}"
  protocol          = "${var.listener_http_protocol}"

  default_action {
    type = "redirect"

    redirect {
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https_static" {
  count = "${var.default_page_static_content == "" ? 0 : 1}"

  load_balancer_arn = "${aws_lb.default.arn}"
  port              = "${var.listener_https_port}"
  protocol          = "${var.listener_https_protocol}"

  ssl_policy = "${var.alb_ssl_policy}"

  certificate_arn = "${local.certificate_arn}"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/html"
      message_body = "${var.default_page_static_content}"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener" "https" {
  count = "${var.default_page_static_content == "" ? 1 : 0}"

  load_balancer_arn = "${aws_lb.default.arn}"
  port              = "${var.listener_https_port}"
  protocol          = "${var.listener_https_protocol}"

  ssl_policy = "${var.alb_ssl_policy}"

  certificate_arn = "${local.certificate_arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.https_default.arn}"
    type             = "forward"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "default" {
  zone_id = "${var.dns_zone_id}"
  name    = "${var.dns_name}"
  type    = "A"

  alias {
    name                   = "${lower(aws_lb.default.dns_name)}"
    zone_id                = "${aws_lb.default.zone_id}"
    evaluate_target_health = true
  }
}
