resource "tls_private_key" "default" {
  count = "${var.request_letsencrypt_certificate ? 1 : 0}"

  algorithm = "RSA"
}

resource "acme_registration" "default" {
  count = "${var.request_letsencrypt_certificate ? 1 : 0}"

  account_key_pem = "${tls_private_key.default.private_key_pem}"
  email_address   = "${var.certificate_email_address}"
}

resource "acme_certificate" "default" {
  count = "${var.request_letsencrypt_certificate ? 1 : 0}"

  account_key_pem = "${acme_registration.default.account_key_pem}"
  common_name     = "${var.certificate_common_name}"

  subject_alternative_names = ["${var.certificate_subject_alternative_names}"]

  dns_challenge {
    provider = "route53"

    config {
      AWS_ACCESS_KEY_ID     = "${var.aws_access_key_id}"
      AWS_SECRET_ACCESS_KEY = "${var.aws_secret_access_key}"
      AWS_DEFAULT_REGION    = "${var.aws_default_region != "" ? var.aws_default_region : var.aws_region}"
      AWS_HOSTED_ZONE_ID    = "${var.dns_zone_id}"
    }
  }
}

resource "aws_iam_server_certificate" "default" {
  count = "${var.request_letsencrypt_certificate ? 1 : 0}"

  name = "${local.resource_name_prefix}-${var.aws_region}-${md5(acme_certificate.default.certificate_pem)}"

  certificate_body  = "${acme_certificate.default.certificate_pem}"
  private_key       = "${acme_certificate.default.private_key_pem}"
  certificate_chain = "${acme_certificate.default.issuer_pem}"

  lifecycle {
    create_before_destroy = true
  }
}
