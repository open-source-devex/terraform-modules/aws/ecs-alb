#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

set -v

CI_VARIABLES=ci-variables.tfvars
OVERRIDES_FILE=ci-overrides.tf

echo '
aws_access_key_id = "string"
aws_secret_access_key = "string"
aws_region = "string"
aws_account_id = "string"
aws_azs = ["list"]
aws_role_ecs_service_arn = "string"
vpc_id = "string"
subnet_ids = ["list"]
ecs_cluster_id = "string"
white_list_cidrs_http = {"localhost" = "127.0.0.1/32"}
white_list_cidrs_https = {"localhost" = "127.0.0.1/32"}
logs_s3_bucket_name = "string"
dns_zone_id = "string"
dns_name = "string"
backend_security_group_id = "string"
default_task_count = 1
' > ${CI_VARIABLES}


echo '
provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}

provider "acme" {
  server_url = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES} .

rm -f ci-*.tf*
