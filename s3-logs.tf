locals {
  new_bucket_name = "${var.logs_s3_bucket_name != "" ? var.logs_s3_bucket_name : "${lower(var.project)}-${lower(var.environment)}-logs"}"
}

resource "aws_s3_bucket" "new" {
  count = "${var.create_log_s3_bucket ? 1 : 0}"

  bucket = "${local.new_bucket_name}"
  acl    = "private"

  force_destroy = "${var.logs_force_destroy}"

  policy = <<EOF
{
  "Id": "${title(var.project)}${title(var.environment)}AllowALBLogsToS3Bucket",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPutObject",
      "Action": "s3:PutObject",
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${local.new_bucket_name}/${local.log_bucket_prefix}/AWSLogs/${var.aws_account_id}/*",
      "Principal": {
        "AWS": [
          "${data.aws_elb_service_account.main.arn}"
        ]
      }
    }
  ]
}
EOF

  lifecycle_rule {
    id      = "${local.resource_name_prefix}"
    prefix  = "${local.log_bucket_prefix}"
    enabled = true

    expiration {
      days = 15
    }
  }

  tags {
    Name        = "Logs"
    Environment = "${var.environment}"
    Project     = "${var.project}"
  }
}

data "aws_elb_service_account" "main" {}

data "aws_s3_bucket" "existing" {
  count = "${var.create_log_s3_bucket ? 0 : 1}"

  bucket = "${var.logs_s3_bucket_name}"
}
