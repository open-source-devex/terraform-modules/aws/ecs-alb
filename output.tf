output "https_listener_arn" {
  value = "${element(compact(concat(aws_lb_listener.https_static.*.arn, aws_lb_listener.https.*.arn)), 1)}"
}

output "https_target_group_arn" {
  value = "${aws_lb_target_group.https_default.*.arn}"
}

output "dns_name" {
  value = "${aws_lb.default.dns_name}"
}

output "zone_id" {
  value = "${aws_lb.default.zone_id}"
}

output "security_group_id" {
  value = "${aws_security_group.default.id}"
}

output "logs_bucket_name" {
  value = "${local.log_bucket}"
}
