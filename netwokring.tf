resource "aws_security_group" "default" {
  name        = "${local.resource_name_prefix}Sg"
  description = "Manage traffic to ${title(var.project)} ${title(var.environment)} ECS ALB"

  vpc_id = "${var.vpc_id}"

  tags {
    Name        = "${title(var.project)} ${title(var.environment)} ECS ALB Security Group"
    Environment = "${var.environment}"
  }
}

locals {
  white_list_cidr_http_keys  = ["${keys(var.white_list_cidrs_http)}"]
  white_list_cidr_https_keys = ["${keys(var.white_list_cidrs_https)}"]
}

resource "aws_security_group_rule" "from_whitelist_to_alb_http" {
  count = "${length(local.white_list_cidr_http_keys)}"

  description = "from ${element(local.white_list_cidr_http_keys, count.index)}"

  type      = "ingress"
  from_port = "${var.listener_http_port}"
  to_port   = "${var.listener_http_port}"
  protocol  = "tcp"

  cidr_blocks = ["${lookup(var.white_list_cidrs_http, element(local.white_list_cidr_http_keys, count.index), "not-found")}"]

  security_group_id = "${aws_security_group.default.id}"
}

resource "aws_security_group_rule" "from_whitelist_to_alb_https" {
  count = "${length(local.white_list_cidr_https_keys)}"

  description = "from ${element(local.white_list_cidr_https_keys, count.index)}"

  type      = "ingress"
  from_port = "${var.listener_https_port}"
  to_port   = "${var.listener_https_port}"
  protocol  = "tcp"

  cidr_blocks = ["${lookup(var.white_list_cidrs_https, element(local.white_list_cidr_https_keys, count.index), "not-found")}"]

  security_group_id = "${aws_security_group.default.id}"
}

resource "aws_security_group_rule" "alb_to_targets" {
  description = "to ECS targets"

  type                     = "egress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = "${var.backend_security_group_id}"

  security_group_id = "${aws_security_group.default.id}"
}

resource "aws_security_group_rule" "alb_from_targets" {
  description = "from ECS targets"

  type                     = "ingress"
  from_port                = 32768
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.default.id}"

  security_group_id = "${var.backend_security_group_id}"
}
