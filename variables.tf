variable "project" {
  description = "Project name"
  default     = "project"
}

variable "environment" {
  description = "Environment name"
  default     = "env"
}

variable "alb_name" {
  description = "The name of the ALB. Default: alb"
  default     = "alb"
}

variable "alb_deletion_protection" {
  description = "Set to false to allow terraform to destroy the ALB"
  default     = true
}

variable "alb_ssl_policy" {
  description = "The SSL policy to set on the HTTPS listener of the ALB"
  default     = "ELBSecurityPolicy-TLS-1-2-2017-01"
}

variable "internal" {
  description = "Flag to set ALB to be internal or public facing. Default: false"
  default     = false
}

variable "white_list_cidrs_http" {
  description = "A map with tuples containing CIDR block and description to add as whitelist rules for HTTP on the ALB security group"
  type        = "map"
  default     = {}
}

variable "white_list_cidrs_https" {
  description = "A map with tuples containing CIDR block and description to add as whitelist rules for HTTPS on the ALB security group"
  type        = "map"
  default     = {}
}

variable "extra_security_groups" {
  description = "A list of security groups to be atatched to the ALB in addition to the white lists"
  type        = "list"
  default     = []
}

variable "dns_zone_id" {
  description = "The ID of the DNS zone where to create a record for the ALB."
}

variable "dns_name" {
  description = "The name for the Alias A record for the ALB. The name on the record will be this variable and the value will be the generated domain name of the ALB."
}

variable "default_page_static_content" {
  description = "When set to none empty string all other default page settings are ignored and a the default page is the content of the string"
  default     = ""
}

variable "default_page_container_image" {
  description = "The default page container image"
  default     = "registry.gitlab.com/open-source-devex/containers/maintenance-page"
}

variable "default_page_container_image_version" {
  description = "The version of the default page container"
  default     = "latest"
}

variable "default_page_container_environment_configuration" {
  description = "The environment configuration for the of the default page container. This valiable expects a list of maps with the format required by the container definition of an ECS task. Example: [{'name': 'FOO', 'value': 'bar'}, ...] (replacing single-quotes by double quotes!)"
  type        = "list"
  default     = []
}

variable "default_page_container_port" {
  description = "The port under which the default page is served by the container (the internal container port)"
  default     = 80
}

variable "default_page_container_cpu" {
  description = "The CPU shares given to the default page container. 1024 means 1 full core, so this variable usually takes fractions of that."
  default     = 0
}

variable "default_page_container_memory" {
  description = "The memory limit set for the default page container in MB"
  default     = 32
}

variable "default_page_health_check_path" {
  description = "The path used for the health check request for the default page"
  default     = "/health"
}

variable "default_page_health_check_matcher" {
  description = "The expected status code for the health check of the default page"
  default     = "200"
}

variable "https_redirect_container_image_version" {
  description = "The version of the HTTP to HTTPS redirection container"
  default     = "latest"
}

variable "backend_security_group_id" {
  description = "The ID of the security group of the ALB's backend services"
}

variable "aws_azs" {
  description = "The Availablity Zones for the ALB"
  type        = "list"
}

variable "ecs_cluster_id" {
  description = "The ID of the ECS cluster"
}

variable "aws_role_ecs_service_arn" {
  description = "The IAM role for the ECS cluster services"
}

variable "default_task_count" {
  description = "The number of tasks to run for each of the default services (https redirection, maintenance page)"
}

variable "create_log_s3_bucket" {
  description = "Set to false to prevent terraform from creating an S3 bucket to hold ALB logs (requires `logs_s3_bucket_name`)."
  default     = true
}

variable "logs_s3_bucket_name" {
  description = "The S3 bucket name to store the logs in, Requered when `create_log_s3_bucket` is set to false."
  default     = ""
}

variable "log_bucket_enabled" {
  default = "true"
}

variable "logs_force_destroy" {
  description = "Set to false to prevent terraform from destrying the S3 bucket with the logs (if bucket is not empty)"
  default     = true
}

variable "aws_account_id" {
  description = "The AWS account id, used to set a policy on the S3 bucket for logs."
}

variable "aws_access_key_id" {
  description = "The AWS account access key id, used to manipulate Route53 records to request SSL certificates, required when `request_letsencrypt_certificate = true`"
  default     = ""
}

variable "aws_secret_access_key" {
  description = "The AWS account secret access key, used to manipulate Route53 records to request SSL certificates, required when `request_letsencrypt_certificate = true`"
  default     = ""
}

variable "aws_default_region" {
  description = "The default AWS region used to manipulate Route53 records to request SSL certificates"
  default     = ""
}

variable "aws_region" {
  description = "The AWS region, used to set a policy on the S3 bucket for logs."
}

variable "alb_idle_timeout" {
  description = "The ALB idle timeout value"
  default     = 30
}

variable "service_minimum_healthy" {
  description = "The minimum percentage of healhy tasks required to be running"
  default     = 100
}

variable "service_deployment_maximum" {
  description = "The maximum percentage of healhy tasks that can be running during deployments, before older tasks must be stopped"
  default     = 200
}

variable "vpc_id" {
  description = "The ID of the VPC"
}

variable "vpc_cidr_block" {
  description = "The CIDR block for VPC"
  default     = "10.0.0.0/16"
}

variable "subnet_ids" {
  description = "A list of subnet IDs to deploy the ALB in"
  type        = "list"
}

variable "target_group_port" {
  description = "The default port for the target group forwarding"
  default     = 80
}

variable "target_group_protocol" {
  description = "The default prootcol for the target group forwarding. Set to HTTPS to have ALB not perform SSL termination."
  default     = "HTTP"
}

variable "listener_http_port" {
  description = "The port of the HTTP listener"
  default     = 80
}

variable "listener_http_protocol" {
  description = "The protocol for the HTTP listener"
  default     = "HTTP"
}

variable "listener_https_port" {
  description = "The port of the HTTPS listener"
  default     = 443
}

variable "listener_https_protocol" {
  description = "The protocol for the HTTPS listener"
  default     = "HTTPS"
}

variable "health_check_healthy_threshold" {
  description = "How many successful health checks need to be performed before considering target healthy"
  default     = 2
}

variable "health_check_unhealthy_threshold" {
  description = "How many successful health checks need to be performed before considering target unhealthy"
  default     = 2
}

variable "health_check_interval" {
  description = "The interval in between health checks"
  default     = 10
}

variable "health_check_timeout" {
  description = "How many secconds to wait before failing health check. Default: 5."
  default     = 5
}

variable "deregistration_delay" {
  description = "The time to wait when deregistering targets from the ALB"
  default     = 5
}

variable "request_letsencrypt_certificate" {
  description = "Flag to disable requesting Letsencrypt certificate for HTTPS listener"
  default     = true
}

variable "certificate_arn" {
  description = "The ARN of a certificate that exists in ACM or IAM, required when `request_letsencrypt_certificate = false`"
  default     = ""
}

variable "certificate_email_address" {
  description = "The email for the requested certificate installed in the ALB's HTTPS listener"
  default     = "nlo@utilus.nl"
}

variable "certificate_common_name" {
  description = "The common name for the requested certificate installed in the ALB's HTTPS listener, required when `request_letsencrypt_certificate = false`"
  default     = ""
}

variable "certificate_subject_alternative_names" {
  description = "List of subject alternative names for the requested certificate installed in the ALB's HTTPS listener, required when `request_letsencrypt_certificate = false`"
  type        = "list"
  default     = []
}
